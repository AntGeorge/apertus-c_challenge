main: main.c
	gcc -o main main.c -Wall -O3

clean:
	rm -f *.o
	rm -f main