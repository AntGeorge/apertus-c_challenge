// SPDX-License-Identifier: GPL-2.0
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#define LINES		3072
#define COLUMNS		4096
#define SENSOR_BITS	12
#define BUCKETS		32
#define MAX_PIXEL_VALUE	4096
#define BYTES_OF_ROW	(COLUMNS * SENSOR_BITS / 8)


struct Histogram {

	struct Channel {
		char *name;
		unsigned int bucket[32];
	} left_up_channel, right_up_channel,
	  left_down_channel, right_down_channel;

// Initialize buckets with zeros and RG/GB pattern
} hist = { {"Red", {0} }, {"Green1", {0} }, {"Green2", {0} }, {"Blue", {0} } };


int main(int argc, char *argv[])
{
	int opt;
	int timing_runs = 1;
	int timing_runs_enable = 0;

	do {
		opt = getopt(argc, argv, ":vf:");

		switch (opt) {

		// Setup for mirror vertically
		case 'v':
			hist.left_up_channel.name = "Green1";
			hist.right_up_channel.name = "Blue";
			hist.left_down_channel.name = "Red";
			hist.right_down_channel.name = "Green2";
			break;
		// Setup timing framework,
		// run the algorithm for *timing_runs* times
		case 'f':
			timing_runs = atoi(optarg);
			if (timing_runs == 0) {
				printf("-f value must be greater than 0\n");
				exit(1);
			}
			timing_runs_enable = 1;
			break;
		case ':':
			printf("This option needs a value\n");
			exit(1);
			break;
		}
	} while (opt != -1);

	FILE *raw_file;

	raw_file = fopen("portrait-gainx2-offset2047-20ms-02.raw12", "rb");

	if (!raw_file) {
		fprintf(stderr, "Can't open the file\n");
		exit(1);
	}

	// Buffer size of one line (12 bits * 4096 items)
	unsigned char buffer[BYTES_OF_ROW];

	unsigned int FirstChannel, SecondChannel;

	clock_t begin, end;
	double time = 0;

	struct Channel *left, *right;

	for (register int c = 0; c < timing_runs; c++) {

		if (timing_runs_enable) {

			// Reset buckets
			for (int i = 0; i < BUCKETS; i++) {

				hist.left_up_channel.bucket[i] = 0;
				hist.right_up_channel.bucket[i] = 0;
				hist.left_down_channel.bucket[i] = 0;
				hist.right_down_channel.bucket[i] = 0;
			}

			// Seek to start
			fseek(raw_file, 0, SEEK_SET);
		}

		begin = clock();

		for (register int i = 0; i < LINES; i++) {

			if (i % 2) {
				left = &hist.left_down_channel;
				right = &hist.right_down_channel;
			} else {
				left = &hist.left_up_channel;
				right = &hist.right_up_channel;
			}

			// Read line
			fread(buffer, BYTES_OF_ROW, 1, raw_file);

			for (register int j = 0; j < BYTES_OF_ROW; j += 3) {

				FirstChannel = buffer[j] << 4 |
					(buffer[j + 1] & 0xF0) >> 4;
				SecondChannel = (buffer[j + 1] & 0x0F) << 8 |
					buffer[j + 2];

				left->bucket[(int)(FirstChannel * (BUCKETS /
					(double) MAX_PIXEL_VALUE))]++;
				right->bucket[(int)(SecondChannel * (BUCKETS /
					(double) MAX_PIXEL_VALUE))]++;
			}
		}

		end = clock();
		time += (double)(end - begin) / CLOCKS_PER_SEC;
	}

	// Print histogram
	printf("%-10s %-10s %-10s %-10s %-10s\n", "bucket",
		hist.left_up_channel.name,
		hist.right_up_channel.name,
		hist.left_down_channel.name,
		hist.right_down_channel.name);

	for (int i = 0; i < BUCKETS; i++)
		printf("%-10i %-10i %-10i %-10i %-10i\n", i,
			hist.left_up_channel.bucket[i],
			hist.right_up_channel.bucket[i],
			hist.left_down_channel.bucket[i],
			hist.right_down_channel.bucket[i]);

	fclose(raw_file);

	if (timing_runs_enable == 1) {
		printf("\n");
		printf("Average time of %d runs is %f sec(s)\n",
		timing_runs, time / timing_runs);
	}

	return 0;
}
