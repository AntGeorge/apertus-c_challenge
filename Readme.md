Qualification Task (**C Challenge T1140**)

The solution of https://lab.apertus.org/T1140

### To build and run
```
make
./main
```

This will read the file **portrait-gainx2-offset2047-20ms-02.raw12** and calculate the histogram.

### Options
`-v`: mirror vertically

`-f number`: number of runs to calculate the average time of executing